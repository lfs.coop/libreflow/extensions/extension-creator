import os
import re
import stat
import shutil
import datetime
import subprocess
import gitlab

from dotenv import load_dotenv
from pprint import pprint

config_file = f'{os.path.dirname(__file__)}\.env'


def remove_readonly(func, path, exc_info):
    """
    Clear readonly file and reattempt the removal
    Use for git data in dir
    """
    
    if func not in (os.unlink, os.rmdir) or exc_info[1].winerror != 5:
        raise exc_info[1]
    os.chmod(path, stat.S_IWRITE)
    func(path)


def interactive_mode():
    '''
    Ask to user, how the script should be executed
    '''

    # Use dot env file if exists
    env_exists = False
    if os.path.exists(config_file):
        env_exists = True
        load_dotenv(dotenv_path=config_file)

        extension_directory = os.getenv('EXTENSION_DIRECTORY')
        author = os.getenv('AUTHOR')
        author_email = os.getenv('AUTHOR_EMAIL')
        initial_version = os.getenv('INITIAL_VERSION')
        src_directory = os.getenv('SRC_DIRECTORY')
        gitlab_token = os.getenv('GITLAB_PERSONAL_TOKEN')

    # Ask for extension name
    extension_name = input("Extension name (libreflow.extensions.yourname): ")
    if extension_name == '':
        print('ERROR: You should name your extension.')
        exit()
    else:
        extension_name = f'libreflow.extensions.{extension_name}'

    # Ask other mandatory inputs if no env file
    if env_exists is False:
        # Where the extension is stored
        extension_directory = input('Extension path (leave empty for current): ')
        if extension_directory == '':
            extension_directory = os.getcwd()
        else:
            extension_directory = os.path.normpath(extension_directory)
            if os.path.isdir(extension_directory) is False:
                print('ERROR: Path is not a dir or valid.')
                exit()
        
        # Author name
        author = input('Author name (default: LFS): ')
        if author == '':
            author = 'LFS'
        
        # Author email
        author_email = input('Author email (default: libreflow@lfs.coop): ')
        if author_email == '':
            author_email = 'libreflow@lfs.coop'

        # Initial version number
        initial_version = input('Initial version (default: 1.0.0): ')
        if initial_version == '':
            initial_version = '1.0.0'

        # Source directory for versioneer
        src_directory = input('Source directory (default: src): ')
        if src_directory == '':
            src_directory = 'src'

    # Ask if extension directory exists, should be removed?
    remove_existing = input('Remove existing (y/n): ').lower().strip() == 'y'

    include_README = True
    include_CHANGELOG = True

    # If gitlab token, ask if the repo should be pushed to remote
    push_repo = False
    if env_exists:
        push_repo = input('Push repo to GitLab? (y/n): ').lower().strip() == 'y'

    script_options = dict(
        extension_dir=extension_directory,
        author=author,
        author_email=author_email,
        initial_version=initial_version,
        src_dir=src_directory,
        gitlab_token=gitlab_token,
        extension_name=extension_name,
        remove_existing=remove_existing,
        include_README=include_README,
        include_CHANGELOG=include_CHANGELOG,
        push_repo=push_repo
    )
    
    return script_options


def create_folders(option):
    '''
    Create extension folder structure
    '''

    # Source dirs
    extension_dirs = [option['src_dir']] + option['extension_name'].replace("-","_").split(".")

    # Base dir
    folder_name = option['extension_name'].replace("libreflow.extensions.", "")

    base_dir = os.path.join(option['extension_dir'], folder_name)

    # Remove if existing option is used and dir exists
    if os.path.exists(base_dir) and option['remove_existing'] == False:
        print("WARNING: Extension folder exists\nEnable 'remove_existing' to force it")
        exit()
    elif os.path.exists(base_dir):
        print('     shutil: rmtree')
        shutil.rmtree(base_dir, onerror=remove_readonly)
    
    # Create dir
    full_dir = os.path.join(base_dir, *extension_dirs)
    print('     os: makedirs')
    os.makedirs(full_dir)

    return extension_dirs, folder_name, base_dir


def create_setup_py(option, base_dir):
    '''
    Configure setup.py file for versioneer
    '''

    # region File content
    SETUP = f"""import setuptools
import versioneer
import os

readme = os.path.normpath(os.path.join(__file__, '..', 'README.md'))
with open(readme, "r", encoding="utf-8") as fh:
    long_description = fh.read()

long_description += '\\n\\n'

changelog = os.path.normpath(os.path.join(__file__, '..', 'CHANGELOG.md'))
with open(changelog, "r", encoding="utf-8") as fh:
    long_description += fh.read()


setuptools.setup(
    
    name="{option['extension_name']}",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    author="{option['author']}",
    author_email="{option['author_email']}",
    description="",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/lfs.coop/libreflow/libreflow_launcher",
    license="LGPLv3+",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Operating System :: OS Independent",
    ],
    keywords="kabaret libreflow",
    install_requires=[],
    python_requires='>=3.7',
    packages=setuptools.find_packages("{option['src_dir']}"),
    package_dir={{"": "{option['src_dir']}"}},
    package_data={{
        '': [
            "*.css",
            '*.png'
        ],
    }},

)
"""
    # endregion

    # Write file
    setup_path = os.path.join(base_dir, "setup.py")
    f = open(setup_path, 'w')
    f.write(SETUP)
    f.close()


def create_readme(base_dir, folder_name):
    '''
    Write a base README file
    '''

    # Define a nice name for extension name
    extension_nice_name = re.sub('[\._-]', ' ', folder_name).title()

    # region README content
    README = f"""# {extension_nice_name}

hello world
"""
    # endregion

    # Write file
    readme_path = os.path.join(base_dir, "README.md")
    f = open(readme_path, 'w')
    f.write(README)
    f.close()


def create_changelog(option, base_dir):
    '''
    Write CHANGELOG document with Semantic Versioning
    '''

    # Get today date
    now = datetime.datetime.now()

    # region CHANGELOG content
    CHANGELOG = f"""# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)[^1].

<!---
Types of changes

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

-->

## [Unreleased]

## [{option['initial_version']}] - {now.year}-{now.month:02d}-{now.day:02d}

### Added

* initial release
"""
    # endregion

    # Write file
    CHANGELOG_path = os.path.join(base_dir, "CHANGELOG.md")

    f = open(CHANGELOG_path, 'w')
    f.write(CHANGELOG)
    f.close()


def create_manifest(option, base_dir):
    '''
    Write MANIFEST file for package building
    '''

    # MANIFEST content
    MANIFEST = ""

    if option['include_README']:
        MANIFEST += "include README.md\n"
    if option['include_CHANGELOG']:
        MANIFEST += "include CHANGELOG.md\n"

    # Write file
    MANIFEST_path = os.path.join(base_dir, "MANIFEST.in")

    f = open(MANIFEST_path, 'w')
    f.write(MANIFEST)
    f.close()


def create_gitlab_ci(base_dir):
    '''
    Create Gitlab CI for auto upload package to PyPi
    when a commit has a tag version assigned.
    '''

    # region GITLAB CI config
    GITLAB_CI ="""stages:
  - deploy


PyPI:
  only:
    refs:
      - tags

  allow_failure: false

  image: python:3
  stage: deploy
  script:
    - pip install -U twine setuptools
    - python setup.py sdist
    - twine upload --username $PYPI_USERNAME --password $PYPI_PASSWORD dist/*

"""
    # endregion

    # Write file
    GITLAB_CI_path = os.path.join(base_dir, ".gitlab-ci.yml")

    f = open(GITLAB_CI_path, 'w')
    f.write(GITLAB_CI)
    f.close()


def create_script(base_dir, extension_dirs):
    '''
    Create extension base script
    '''

    # Create empty init file
    current_dir = base_dir
    for d in extension_dirs:
        current_dir = os.path.join(current_dir, d)
        f = open(os.path.join(current_dir, "__init__.py"), "w")
        f.close()


    # region EXTENSION script content
    EXAMPLE_FILE = """from kabaret import flow
from kabaret.flow.object import _Manager

class MyAction(flow.Action):
    _MANAGER_TYPE = _Manager


def create_action(parent):
    if 'extendable_object' in parent.oid():
        r = flow.Child(MyAction)
        r.name = 'dynamic_action'
        return r


def install_extensions(session):
    return {
        "demo": [
            create_action,
        ]
    }

"""
    # endregion

    # Write file
    f = open(os.path.join(current_dir, "__init__.py"), "w")
    f.write(EXAMPLE_FILE)
    f.close()


def create_setup_cfg(base_dir, extension_dirs):
    '''
    Create setup.cfg file for versioneer
    '''

    # region SETUP CONFIG content
    SETUP_CFG = f"""[versioneer]
VCS = git
style = pep440
versionfile_source = {"/".join(extension_dirs)}/_version.py
versionfile_build = {"/".join(extension_dirs[1:])}/_version.py
tag_prefix =
"""
    # endregion

    # Write file
    SETUP_CFG_path = os.path.join(base_dir, "setup.cfg")

    f = open(SETUP_CFG_path, 'w')
    f.write(SETUP_CFG)
    f.close()


def install_versioneer(base_dir):
    '''
    Initialize a git repo and install versioneer for version management
    '''

    # Set terminal to extension dir
    os.chdir(base_dir)
    # Create git repo

    print('     Git: Init')
    subprocess.call(["git", "init", "-b", "main"])
    # Install versioneer to project
    print('     Versioneer: Install')
    subprocess.call(["versioneer", "install", "--vendor"])


def update_gitignore(base_dir):
    '''
    Create a .gitignore file for don't push caching and local-only files
    '''

    # region GITIGNORE content
    GITIGNORE = f"""*.pyc
*.pyo
__pycache__/
build/
dist/
docs/_build/
*.egg-info
*.sublime-workspace
.idea
.vscode
*.venv
.usr
Pipfile*
"""
    # endregion

    # Write file
    GITIGNORE_path = os.path.join(base_dir, ".gitignore")

    f = open(GITIGNORE_path, 'w')
    f.write(GITIGNORE)
    f.close()


def create_gitlab_repo(option, base_dir, folder_name):
    '''
    Create remote gitlab repo and push base content
    '''

    category_name = folder_name.split('.')[0]
    project_name = folder_name.split('.')[1]

    # Log in with token
    print('     GitLab: Log in')
    gl = gitlab.Gitlab(private_token=option['gitlab_token'])
    print('     GitLab: Logged')

    # Get extensions group
    print('     GitLab: Get extensions group')
    extensions = gl.groups.get(63536196)

    # Get or create category group
    category_group = None
    category_exists = False
    for category in extensions.subgroups.list():
        if category.name == category_name:
            print('     GitLab: Get category group')
            category_group = gl.groups.get(category.id)
            category_exists = True
            break
    
    if category_group is None:
        print('     GitLab: Create category group')
        category_group = gl.groups.create({
            'name': category_name,
            'path': category_name,
            'parent_id': extensions.id,
            'visibility': 'public'
        })
    
    # Get or create extension repo
    project_entity = None
    project_exists = False
    if category_exists:
        for project in category_group.projects.list():
            if project.name == project_name:
                print('     GitLab: Get extension repo')
                project_entity = gl.projects.get(project.id)
                project_exists = True
                break
    
    if project_entity is None:
        print('     GitLab: Create extension repo')
        project_entity = gl.projects.create({
            'name': project_name,
            'namespace_id': category_group.id,
            'visibility': 'public'
        })
    
    # Setup protected tag for CI
    print('     GitLab: Set up protected tag')
    protected_tags = [p_tag.name for p_tag in project_entity.protectedtags.list()]
    if project_exists is False or '*' not in protected_tags:
        project_entity.protectedtags.create({'name': '*', 'create_access_level': '40'}) # 40 for Maintainer role
   
    # Get repository remote http URL
    remote_url = project_entity.http_url_to_repo

    # Set up remote to local repo
    os.chdir(base_dir)
    print('     Git: Set up remote')
    subprocess.call(["git", "remote", "add", "origin", remote_url])
    print('     Git: Fetch')
    subprocess.call(["git", "fetch"])

    # Commit and push to remote
    print('     Git: Stage changes')
    subprocess.call(["git", "add", "."])
    print('     Git: Commit')
    subprocess.call(["git", "commit", "-m", "chore: init"])
    print('     Git: Set upstream branch')
    subprocess.call(["git", "branch", "--set-upstream-to", "origin/main"])

    # If remote repo already existing, allow force push and force push with lease
    if option['remove_existing'] is True and category_exists is True and project_exists is True:
        print('     GitLab: Allow force push to main branch')
        main_branch = project_entity.protectedbranches.get('main')
        main_branch.allow_force_push = True
        main_branch.save()

        print('     Git: Force push with lease')
        subprocess.call(["git", "push", "--force-with-lease"])
    else:
        print('     Git: Push')
        subprocess.call(["git", "push", "--set-upstream", "origin", "main"])


def main():
    script_options = interactive_mode()
    print('\n######\n')

    print('(1/11) Create extension folder structure')
    extension_dirs, folder_name, base_dir = create_folders(script_options)
    
    print('(2/11) Create setup.py for versioneer')
    create_setup_py(script_options, base_dir)

    print('(3/11) Create README')
    create_readme(base_dir, folder_name)

    print('(4/11) Create CHANGELOG')
    create_changelog(script_options, base_dir)

    print('(5/10) Create MANIFEST')
    create_manifest(script_options, base_dir)

    print('(6/10) Create CI')
    create_gitlab_ci(base_dir)

    print('(7/10) Create base script')
    create_script(base_dir, extension_dirs)

    print('(8/10) Create setup.cfg for versioneer')
    create_setup_cfg(base_dir, extension_dirs)

    print('(9/10) Git init and versioneer install')
    install_versioneer(base_dir)
    
    print('(10/10) Create GITIGNORE')
    update_gitignore(base_dir)

    if script_options['push_repo'] is True:
        print('(11/11) Create Gitlab repo and push')
        create_gitlab_repo(script_options, base_dir, folder_name)
    
    print('\nCOMPLETE')


if __name__ == "__main__":
    main()
