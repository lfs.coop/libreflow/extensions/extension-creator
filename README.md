# Libreflow extension creator

A Python script that prepares everything you need to start creating a new libreflow extension.
Including the folder structure, git repository and versioneer

## .env parameters

* `EXTENSION_DIRECTORY`: Where your extensions are stored on your machine
* `AUTHOR`: Author name that will be shown on the PyPi package page
* `AUTHOR_EMAIL`: Author's email address for support
* `INITIAL_VERSION`: Initial version number of your extension
* `SRC_DIRECTORY`: Source directory for versioneer
* `GITLAB_PERSONAL_TOKEN`: Used to create and push the git repo to the LFS GitLab

## Dependencies

[python-gitlab](https://pypi.org/project/python-gitlab/)
